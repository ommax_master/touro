const gulp        = require('gulp')
const concat      = require('gulp-concat')
const uglify      = require('gulp-uglify')
const include     = require('gulp-include')
const livereload  = require('gulp-livereload')
const babel       = require('gulp-babel')

const paths  = {
    scriptsMaster: {
        src:  "../extensions_local/touro_master/Build/JavaScript/",
        dest: "../extensions_local/touro_master/Resources/Public/JavaScript"
    }
}

function scriptsMaster() {
    return gulp.src(paths.scriptsMaster.src+'App.js')
        .pipe(include())
        .pipe(babel())
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(paths.scriptsMaster.dest))
        .pipe(uglify())
        .pipe(babel())
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest(paths.scriptsMaster.dest))
        .pipe(livereload())
}
function scriptsForm() {
    return gulp.src(paths.scripts.src+'App.js')
        .pipe(jsImport({
            hideConsole: true
        }))
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(uglify())
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest(paths.scripts.dest))
        .pipe(livereload())
}

function watch(cb){
    livereload.listen();
    gulp.watch([paths.scriptsMaster.src+'**/*.js'],scriptsMaster)
    cb()
}

exports.scriptsMaster = scriptsMaster
exports.watch = watch

const build = gulp.series(watch, gulp.parallel(scriptsMaster))

exports.default = build