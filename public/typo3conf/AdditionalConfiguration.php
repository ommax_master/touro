<?php
/*
 * This file is part of the package bw3rk/touro_master.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

$sApplicationContext = \TYPO3\CMS\Core\Core\Environment::getContext();

switch ($sApplicationContext) {
    case 'Development':
        $custonConfiguration = [
            'FE' => [
                'debug' => 1
            ],
            'BE' => [
                'debug' => 1
            ],
            'SYS' => [
                'devIPmask' => '*',
                'displayErrors' => 1,
                'systemLogLevel' => 0,
                'exceptionalErrors' => 'E_ALL',
                'generateApacheHtaccess' => false
            ],
            'EXTENSIONS' => [
                'backend' => [
                    'loginFootnote' => "Entwicklungsumgebung"
                ]
            ],
            'DB' => [
                'Connections' => [
                    'Default' => [
                        'dbname' => 'usr_p574969_1',
                        'host' => 'db001318.mydbserver.com',
                        'user'=> 'p574969',
                        'password' => 'voqm8qtsV!Fz'
                    ]
                ]
            ]
        ];
        break;

    default:
        $custonConfiguration = [
            'EXTENSIONS' => [
                'backend' => [
                    'loginFootnote' => "Stage"
                ]
            ]
        ];

}

$GLOBALS['TYPO3_CONF_VARS'] = array_replace_recursive($GLOBALS['TYPO3_CONF_VARS'], (array)$custonConfiguration);
