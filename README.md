# touro Pretotype
## TYPO3 CMS Base Distribution

Get going quickly with TYPO3 CMS.

## Prerequisites

* PHP 7.2 (oder höher)
* [Composer](https://getcomposer.org/download/)


TYPO3 Installation umgesetzt mit composer und lokalen (DEV) Extensions im selben GIT Repo.
Folgende Thirdparty-Extensions sind enthalten:

* ```bk2k/bootstrap-package```
* ```fluidtypo3/flux```
* ```fluidtypo3/vhs```
* ```lochmueller/sourceopt```
* ```helhum/typo3-console```


Onepager Grid mit bk2k/bootstrap-package und eigenem Sitepackage umgesetzt.
CSS Files werden on the fly aus SCSS gerendert. (Ist im Bootstrap Package enthalten!)
JacaScript wird lokal mit GULP und babel gerendert. Files werden mittels concat zusammengefasst und mit uglify minifiziert. 
Es liegen immer minifizierte und concatenate Versionen vor.


Sticky Navigation kann im Template angepasst werden. Wird als CSS Klasse mittels Bootstrap Package umgesetzt. 
Beide Varianten sollten schnell eingestellt werden können. Allerdings geht das nicht über das Backend.
„Jetzt mieten" als stickt Element am Ende der Seite 

in mobiler Version – Desktopversion ohne sticky Element.
Top-Navigation sticky auf Desktop und mobil, Möglichkeit bereitstellen, die sticky-eigenschaft bei top navigation 
auszuschließen sofern mobil zu wenig platz übrig sein sollte.


## Sitepackage

Das SitePackage liegt im Projekt-Ordner ```./extensions_local/``` mit dem Namen ```touro_master```.
Eingebunden wird es über composer mittels DEv-Symlink.

### Setup
**SASS:**


SASS Rendering in CSS erfolgt per TYPO3 automatisch. Die SASS Dateien liegen im Sitepackage 
Ordner ```extensions_local/touro_master/Resources/Public/Scss```.


***JavaScript***


JavaScript wird mit GULP zusammengefasst und minifiziert. Die Daten für den GULP-Prozess liegen im Projekt-Ordner
```./build/```. Dort sind die nötigen Node-JS Dateinen und das Gulpfile. Ein ```nmp install``` im Ordner installiert 
die nötigen Node Libs und mit ```gulp``` wird dann der Prozess gestartet.


***Mailversand***


Das Mail-Template ist hardcodiert in der Verarbeitenden Middleware. Die entsprechende Middleware Klasse liegt im Sitepackage 
unter ```extensions_local/touro_master/Classes/Middleware/FrontendXhrRequestMiddleware.php```


Es ist zu empfehlen den Versand über einen SMTP Server laufen zu lassen. Die Daten dazu entweder über die TYPO3 Settings 
im Backend einrichten, oder entsprechen in der ```AdditionalConfiguration.php```.


***Datenbank***


Ein Datenban-Dump liegt im Projekt-Ordner unter ```./mysql/```.


## Formular

Für das Formular werden folgende Tabellen in der Datenbank verwendet:

* tx_touromaster_formdata
* tx_touromaster_manufacturer
* tx_touromaster_voucher


***tx_touromaster_formdata***

In dieser Tabelle werden die Kundendaten persistiert. Die Daten werden nach dem Klick auf [Weiter] im Step 2 geschrieben. 
Der Gutschein wird ergänzt, sobald der User in Step 3 auf [Gutschein zusenden] klickt. Der Datenaustausch erfolgt 
asynchron mittels Ajax Call. 


Es werden somit Userdaten auch erfassz, wenn der User alle Daten eingetragen hat, aber in Schritt 3 ***nicht*** auf Absenden klickt.


***tx_touromaster_voucher***


In dieser Tabelle sind die Voucher Daten. Sobald ein Voucher per E-Mail versendet wurde, wird der User Datensatz um diese 
Info erweitert und der entsprechende Voucher Datensatz aus der Tabelle ```tx_touromaster_voucher``` gelöscht.  


***tx_touromaster_manufacturer***

In dieser Tabelle liegen die Automarken und Modell Daten, welche die Select-Boxen befüllen.



