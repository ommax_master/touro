APP_BASE = {
    conf: {},
    init: function() {
        APP_SCREEN.init();
        APP_TRAILER.init();
        APP_FORM.init();
    }
}