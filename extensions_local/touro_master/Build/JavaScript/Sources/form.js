APP_FORM = {
    conf: {
        login: {
            id: "frm-login",
            alert: "alert-wrapper",
            button: "frm-login-submit"
        },
        rent: {
            url: "/jetzt-mieten/",
            from: "manufacturer",
            form: {
                id: "frm-rent"
            },
            select: {
                step1: "manufacturer",
                step2: "model",
                text: "Modellreihe",
                value: "none"
            }
        }
    },
    init: function () {
        const obj = this;
        const cnf = obj.conf;
        const frmLogin = $("#" + cnf.login.id);
        const frmRent = $("#" + cnf.rent.form.id);
        if (frmLogin.length) {
            obj.initLoginForm();
        }
        if (frmRent.length) {
            obj.initRentForm();
        }
        document.addEventListener("keydown", function (event) {
            if (event.key === "F13") {
                $("input#firstname").val("Max");
                $("input#surname").val("Mustermann");
                $("input#email").val("frank@klar-it.info");
                $("input#zip").val("12345");
                $("input#town").val("Musterstadt");
                $("input#street").val("Hauptstr.");
                $("input#street_number").val("1");
            }
        });
    },
    initLoginForm: function () {
        const obj = this;
        const cnf = obj.conf;
        $("#" + cnf.login.id).submit(function (event) {
            event.preventDefault();
            obj.setButtonDisabled("#" + cnf.login.button);
            const form = $(this);
            const url = form.attr('action');
            const posting = $.post(url, form.serialize());
            posting.done(function (response) {
                obj.removeButtonDisabled("#" + cnf.login.button);
                obj.loginAlert();
            });
            posting.fail(function () {
                obj.removeButtonDisabled("#" + cnf.login.button);
                obj.loginAlert();
            });
        });
    },
    initRentForm: function () {
        const obj = this;
        const cnf = obj.conf;
        const jqxhr = $.post(
            cnf.rent.url, {from: cnf.rent.select.step1});
        jqxhr.done(function (response) {
            $(response.data).each(function (iId, oMan) {
                const option = $('<option>').val(oMan.manufacturer).html(oMan.manufacturer);
                const select = $("#manufacturer");
                select.append(option);
            }).promise().done(function () {
                const select = $("#manufacturer");
                select.on('change', function (o) {
                    const selected = $("#manufacturer option:selected").text();
                    const posting = $.post(cnf.rent.url, {selected: selected, from: cnf.rent.select.step2});
                    posting.done(function (response) {
                        $("#model").html("").append($('<option>').val(cnf.rent.select.value).html(cnf.rent.select.text));
                        $(response.data).each(function (iId, oMod) {
                            const option = $('<option>').val(oMod.model).html(oMod.model);
                            const select = $("#model");
                            select.append(option);
                        });
                    });
                });
            });
        });
        $("#frm-rent").submit(function (event) {
            event.preventDefault();
            const form = $(this);
            const url = form.attr('action');
            const posting = $.post(url, form.serialize());
            posting.done(function (response) {
                $("input#hidden-field-from").val("done");
                $("div#formular").addClass("d-none");
                $("div#respone").removeClass("d-none");
            });
        });
    },
    nextStep: function (destination) {
        const obj = this;
        const cnf = obj.conf;
        const form = document.getElementById('frm-rent');
        const frmRequired = $("input.required");
        const tab1 = $('#nav-rent #nav-tab-1');
        const tab2 = $('#nav-rent #nav-tab-2');
        const tab3 = $('#nav-rent #nav-tab-3');
        if (destination === "Fahrzeug") {
            if (form.checkValidity()) {
                frmRequired.removeClass("is-invalid").addClass("is-valid");
                tab1.removeClass("invalid").addClass("valid");
                tab2.removeClass("disabled").attr('aria-disabled', 'false').tab('show');
            } else {
                frmRequired.addClass("is-invalid");
                tab1.addClass("invalid");
            }
        }
        if (destination === "Werkstatt") {
            var chassis = $("input#chassis_number"),
                chassisValue = document.getElementById("chassis_number").value;
            let validInput = true;
            if (!chassisValue.length || chassisValue.length > 17 || chassisValue.length < 17) {
                tab2.addClass("invalid");
                chassis.addClass("is-invalid");
                validInput = false;
            } else {
                chassis.removeClass("is-invalid").addClass("is-valid");
                validInput = true;
            }

            var manufacturer = $("#manufacturer"),
                model = $("#model");
            let validInputSelect = true;
            if (manufacturer.val() === "none") {
                manufacturer.addClass("is-invalid");
                validInputSelect = false;
            } else {
                manufacturer.removeClass("is-invalid").addClass("is-valid");
                validInputSelect = true;
            }

            if (model.val() === "none") {
                model.addClass("is-invalid");
                validInputSelect = false;
            } else {
                model.removeClass("is-invalid").addClass("is-valid");
                validInputSelect = true;
            }

            if (form.checkValidity() && validInput === true && validInputSelect === true) {

                frmRequired.removeClass("is-invalid").addClass("is-valid");
                tab2.removeClass("invalid").addClass("valid");
                tab3.removeClass("disabled").attr('aria-disabled', 'false').tab('show');
                const form = $("form#frm-rent");
                const url = form.attr('action');
                const posting = $.post(url, {
                    firstname: $("input#firstname").val(),
                    surname: $("input#surname").val(),
                    email: $("input#email").val(),
                    street: $("input#street").val(),
                    street_number: $("input#street_number").val(),
                    zip: $("input#zip").val(),
                    town: $("input#town").val(),
                    chassis_number: $("input#chassis_number").val(),
                    manufacturer: $("#manufacturer option:selected").text(),
                    model: $("#model option:selected").text(),
                    from: $("input#hidden-field-from").val()
                });
                posting.done(function (response) {
                    $("input#hidden-field-from").val("voucher");
                    $("div.tab-submit-text-wrapper.d-none").removeClass("d-none");
                    tab3.addClass("invalid");
                });
            }
        }
    },
    loginAlert: function () {
        const obj = this;
        const cnf = obj.conf;
        $("#" + cnf.login.alert).removeClass("d-none");
    },
    setButtonDisabled: function (target) {
        const btn = $(target);
        btn.attr({disabled: "disabled"});
    },
    removeButtonDisabled: function (target) {
        const btn = $(target);
        btn.removeAttr("disabled");
    }
}