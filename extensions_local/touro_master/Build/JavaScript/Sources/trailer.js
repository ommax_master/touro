APP_TRAILER = {
    conf: {
        target: {
            init: ".trailer"
        },
        icon: {
            class: ".trailer .icon-info"
        },
        content: {
            class: ".trailer .content-info"
        }
    },
    init: function () {
        const obj = this;
        const cnf = obj.conf;
        if ( $(cnf.target.init).length != 0 ) {
            $.each($(cnf.icon.class), function (){
                const oImg = $(this);
                const oData = oImg.data();
                const oTarget = $(oData.target);
                oImg.on('click', function (){
                    if ( oTarget.hasClass("d-none")) {
                        obj.setAllContentDisplayNone();
                        oTarget.removeClass("d-none");
                    } else {
                        obj.setAllContentDisplayNone();
                    }
                });
            });
            $("img.trailer-img").on('click',function(){
                obj.setAllContentDisplayNone();
            });
        }
    },
    setAllContentDisplayNone: function() {
        const cnf = this.conf;
        $(cnf.content.class).addClass("d-none");
    }
}