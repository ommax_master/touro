APP_SCREEN = {
    conf: {
        target: "body",
        body: {
            item: "html",
            js: "js",
            nojs: "nojs"
        },
        viewport: {
            xxl: {
                name: "desktop-xl",
                class: "desktop desktop-extralarge xxl"
            },
            xl: {
                name: "desktop-lg",
                class: "desktop desktop-large xl"
            },
            lg: {
                name: "desktop",
                class: "desktop lg"
            },
            md: {
                name: "desktop",
                class: "desktop tablet md"
            },
            sm: {
                name: "tablet",
                class: "tablet sm"
            },
            xs: {
                name: "phone",
                class: "phone xs"
            }
        }
    },
    viewport: null,
    setViewport: function(sType) {
        this.viewport = sType;
    },
    getViewport: function() {
        return this.viewport;
    },
    init: function() {
        obj = this;
        cnf = obj.conf;
        obj.setJsToHtml();
        obj.initResize();
    },
    setJsToHtml: function () {
        cnf = this.conf;
        $(cnf.body.item).removeClass(cnf.body.nojs).addClass(cnf.body.js);
    },
    initResize: function() {
        var obj = this;
        $(window).bind("resize",function (ev) {
            obj.initViewport();
        });
        obj.initViewport();
    },
    initViewport: function() {
        var obj = this;
        var cnf = obj.conf;
        var iWidth = $(window).width();
        obj.removeAllClassesByElementTarget(cnf.target);
        if (iWidth >= 1400) {
            obj.setViewport(cnf.viewport.xxl.name);
            obj.addClassInElement(cnf.target,cnf.viewport.xxl.class);
            obj.initXL();
        } else if (iWidth >= 1200) {
            obj.setViewport(cnf.viewport.xl.name);
            obj.addClassInElement(cnf.target,cnf.viewport.xl.class);
            obj.initLG();
        } else if (iWidth >= 992) {
            obj.setViewport(cnf.viewport.lg.name);
            obj.addClassInElement(cnf.target,cnf.viewport.lg.class);
            obj.initLG();
        } else if (iWidth >= 768) {
            obj.setViewport(cnf.viewport.md.name);
            obj.addClassInElement(cnf.target,cnf.viewport.md.class);
            obj.initMD();
        } else if (iWidth >= 576) {
            obj.setViewport(cnf.viewport.sm.name);
            obj.addClassInElement(cnf.target,cnf.viewport.sm.class);
            obj.initSM();
        } else if (iWidth < 576) {
            obj.setViewport(cnf.viewport.xs.name);
            obj.addClassInElement(cnf.target,cnf.viewport.xs.class);
            obj.initXS();
        }
    },
    initXL: function () {
        var obj = this;
    },
    initLG: function () {
        var obj = this;
    },
    initMD: function () {
        var obj = this;
    },
    initSM: function () {
        var obj = this;
    },
    initXS: function () {
        var obj = this;
    },
    removeAllClassesByElementTarget: function(sTarget) {
        var obj = this;
        var cnf = obj.conf;
        $.each(cnf.viewport, function(sName,oViewport){
            $(sTarget).removeClass(oViewport.class);
        });
    },
    removeClassInElement: function(sTarget,sClass) {
        $(sTarget).removeClass(sClass);
    },
    addClassInElement: function (sTarget,sClass) {
        $(sTarget).addClass(sClass);
    }
}