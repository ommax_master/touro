<?php

/*
 * This file is part of the package bw3rk/touro_master.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

/***************
 * Make the extension configuration accessible
 */
$extensionConfiguration = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
    \TYPO3\CMS\Core\Configuration\ExtensionConfiguration::class
);
$bootstrapPackageConfiguration = $extensionConfiguration->get('touro_master');

/***************
 * PageTS
 */

// Add Content Elements
if (!(bool) $bootstrapPackageConfiguration['disablePageTsContentElements']) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:touro_master/Configuration/TsConfig/Page/ContentElement/All.tsconfig">');
}

// Add BackendLayouts for the BackendLayout DataProvider
if (!(bool) $bootstrapPackageConfiguration['disablePageTsBackendLayouts']) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:touro_master/Configuration/TsConfig/Page/Mod/WebLayout/BackendLayouts.tsconfig">');
}

// Add Page TsConfig
if (!(bool) $bootstrapPackageConfiguration['disablePageTs']) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:touro_master/Configuration/TsConfig/Page/All.tsconfig">');
}

/***************
 * Register custom EXT:form configuration
 */
//if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('form')) {
//    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(trim('
//        module.tx_form {
//            settings {
//                yamlConfigurations {
//                    120 = EXT:touro_master/Configuration/Form/FormLogin.yaml
//                }
//            }
//        }
//        plugin.tx_form {
//            settings {
//                yamlConfigurations {
//                    120 = EXT:touro_master/Configuration/Form/FormLogin.yaml
//                }
//            }
//        }
//    '));
//}

/***************
 * UserTS
 */

// Add User TsConfig
if (!(bool) $bootstrapPackageConfiguration['disableUserTs']) {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:touro_master/Configuration/TsConfig/User/All.tsconfig">');
}

// Ass Flux / Fluid ContentElements
\FluidTYPO3\Flux\Core::registerProviderExtensionKey('BW3rK.TouroMaster', 'Content');

$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
    \TYPO3\CMS\Core\Imaging\IconRegistry::class
);
$iconRegistry->registerIcon(
    'tx-touro-master', // Icon-Identifier, e.g. tx-myext-action-preview
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:touro_master/Resources/Public/Icons/Extension.png']
);
