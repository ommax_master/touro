<?php

namespace BW3rK\TouroMaster\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use ScssPhp\ScssPhp\Formatter\Debug;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Http\Stream;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Client\ClientInterface;
use TYPO3\CMS\Install\Controller\MaintenanceController;
use TYPO3\CMS\Adminpanel\Controller\MainController;
use TYPO3\CMS\Core\Mail\MailMessage;


class FrontendXhrRequestMiddleware implements MiddlewareInterface {


    public function __construct()
    {
        //
    }

    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Server\RequestHandlerInterface $handler
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);
        $queryParams = $request->getQueryParams();
        $method = $request->getMethod();
        $parsedBody = $request->getParsedBody();

        #$this->senMail();

#DebuggerUtility::var_dump(['voucher'=>$this->updateVoucher(1, "testo")]);die;
        if ( isset($method) && $method == 'POST' && $parsedBody !== null ) {
            switch ( isset($parsedBody['from']) ? $parsedBody['from'] : "" ) {
                case 'manufacturer':
                    $data = [
                        'data' => $this->getAllManufacturer(),
                        'status' => "ok"
                    ];
                    break;

                case 'model':
                    $selected = isset($parsedBody['selected']) ? $parsedBody['selected'] : "";
                    $data = [
                        'selected' => $selected,
                        'data' => $this->getModelByManufacturer( $selected ),
                        'status' => "ok"
                    ];
                    break;
                case 'rent':
                    $aInsert = [
                        'firstname' => $parsedBody['firstname'],
                        'email' => $parsedBody['email'],
                        'surname' => $parsedBody['surname'],
                        'zip' => $parsedBody['zip'],
                        'town' => $parsedBody['town'],
                        'street' => $parsedBody['street'],
                        'street_number' => $parsedBody['street_number'],
                        'manufacturer' => $parsedBody['manufacturer'],
                        'model' => $parsedBody['model'],
                        'chassis_number' => $parsedBody['chassis_number'],
                        'tstamp' => time()
                    ];

                    $data = [
                        'parsedBody' => $parsedBody,
                        'insert' => $aInsert,
                        'voucher' => $this->getNextVoucher(),
                        'execute' => $this->storeFormData($aInsert),
                        'status' => "ok"
                    ];
                    break;

                case 'voucher':
                    $aVoucher = $this->getNextVoucher();
                    $this->deleteVoucher($aVoucher[0]['uid']);
                    $res = $this->updateFormData($aVoucher[0]['voucher'], $parsedBody['email']);
                    $this->senMail(
                        $parsedBody['email'],
                        sprintf("%s %s", $parsedBody['firstname'], $parsedBody['surname']),
                        $aVoucher[0]['voucher']
                    );
                    $aInsert = [

                    ];
                    $data = [
                        'execute' => $res,
                        'voucher' => $aVoucher[0],
                        'email' => $parsedBody['email'],
                        'message' => "remove uid by delete from tx_touromaster_voucher!",
                        'status' => "pending"
                    ];
                    break;

                case 'login':
                default:
                    $data = [
                        'message' => "thers no place like 127.0.0.1",
                        'status' => "pending"
                    ];
                    break;

            }

            $body = $response->getBody();
            $body->rewind();
            $content = json_encode($data);
            $body = new Stream('php://temp', 'rw');
            $body->write($content);

            $response = $response->withHeader('content-type', 'application/json; charset=utf-8')->withBody($body);
        }

        return $response;
    }

    private function getModelByManufacturer($manufactorer) {
        /** @var \TYPO3\CMS\Core\Database\Query\QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_touromaster_manufacturer')->createQueryBuilder();
        $statement = $queryBuilder->select('model')->from('tx_touromaster_manufacturer')->where("manufacturer='".$manufactorer."'")->orderBy('model')->execute();

        return $statement->fetchAll();
    }

    private function getAllManufacturer() {
        /** @var \TYPO3\CMS\Core\Database\Query\QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_touromaster_manufacturer')->createQueryBuilder();
        $statement = $queryBuilder->select('manufacturer')->from('tx_touromaster_manufacturer')->groupBy('manufacturer')->orderBy('manufacturer')->execute();

        return $statement->fetchAll();
    }

    private function getNextVoucher() {
        /** @var \TYPO3\CMS\Core\Database\Query\QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_touromaster_voucher')->createQueryBuilder();
        $statement = $queryBuilder->select('uid','voucher')->from('tx_touromaster_voucher')->orderBy('uid')->setMaxResults(1);

        return $statement->execute()->fetchAll();
    }

    private function deleteVoucher($uid=0) {
        /** @var \TYPO3\CMS\Core\Database\Query\QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_touromaster_voucher')->createQueryBuilder();
        $statement = $queryBuilder->delete('tx_touromaster_voucher')->where(sprintf("uid=%d",$uid));

        return $statement->execute();
    }

    private function storeFormData($aInsert=[]) {
        /** @var \TYPO3\CMS\Core\Database\Query\QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_touromaster_formdata')->createQueryBuilder();
        $statement = $queryBuilder->insert('tx_touromaster_formdata')->values($aInsert);

        return $statement->execute();
    }

    private function updateFormData($voucher="",$email="") {
        /** @var \TYPO3\CMS\Core\Database\Query\QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_touromaster_formdata')->createQueryBuilder();
        $statement = $queryBuilder->update('tx_touromaster_formdata')->where($queryBuilder->expr()->eq('email', $queryBuilder->createNamedParameter($email)))->set("voucher",$voucher);

        return $statement->execute();
    }

    private function senMail($email="", $name="", $voucher="") {
        $sHtml = sprintf(
            "<h1 style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 15px'>Vielen Dank fÃ¼r Ihr Interesse am Service von TOURO</h1>".
            "<p style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px'>Als kleine EntschÃ¤digung fÃ¼r die Unannehmlichkeiten...</p>".
            "<p style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px'>Gutschein-Code: <pre>%s</pre></p>".
            "<p style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px'>Wir wÃ¼nschen Ihnen noch einen angenehmen Tag,<br><strong>Ihr TOURO Team</strong>.</p>"
            ,$voucher
        );
        /** @var \TYPO3\CMS\Core\Mail\MailMessage $mail */
        $mail = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
        $mail->from(new \Symfony\Component\Mime\Address('noreply@kitssl.de', 'TOURO'))
            ->replyTo(new \Symfony\Component\Mime\Address('info@blindwerk.de', 'Blindw3rk - neue medien'))
            ->to(new \Symfony\Component\Mime\Address($email, $name))
            ->subject('Ihre Nachricht an TOURO')
            ->html($sHtml)
            ->send();
    }
}