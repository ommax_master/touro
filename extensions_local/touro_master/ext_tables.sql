CREATE TABLE tx_touromaster_formdata (
    uid int(11) unsigned NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,

    firstname varchar(125) DEFAULT '' NOT NULL,
    surname varchar(125) DEFAULT '' NOT NULL,
    email varchar(255) DEFAULT '' NOT NULL,
    street varchar(255) DEFAULT '' NOT NULL,
    street_number varchar(65) DEFAULT '' NOT NULL,
    zip varchar(65) DEFAULT '' NOT NULL,
    town varchar(255) DEFAULT '' NOT NULL,
    manufacturer varchar(255) DEFAULT '' NOT NULL,
    model varchar(255) DEFAULT '' NOT NULL,
    chassis_number varchar(255) DEFAULT '' NOT NULL,
    voucher varchar(255) DEFAULT '' NOT NULL,

    tstamp int(11) unsigned DEFAULT '0' NOT NULL,
    crdate int(11) unsigned DEFAULT '0' NOT NULL,
    cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
    deleted smallint unsigned DEFAULT '0' NOT NULL,
    hidden smallint unsigned DEFAULT '0' NOT NULL,
    starttime int(11) unsigned DEFAULT '0' NOT NULL,
    endtime int(11) unsigned DEFAULT '0' NOT NULL,
    sorting int(11) DEFAULT '0' NOT NULL,

    sys_language_uid int(11) DEFAULT '0' NOT NULL,
    l10n_parent int(11) unsigned DEFAULT '0' NOT NULL,
    l10n_diffsource mediumblob NULL,

    t3ver_oid int(11) unsigned DEFAULT '0' NOT NULL,
    t3ver_id int(11) unsigned DEFAULT '0' NOT NULL,
    t3ver_wsid int(11) unsigned DEFAULT '0' NOT NULL,
    t3ver_label varchar(255) DEFAULT '' NOT NULL,
    t3ver_state smallint DEFAULT '0' NOT NULL,
    t3ver_stage int(11) DEFAULT '0' NOT NULL,
    t3ver_count int(11) unsigned DEFAULT '0' NOT NULL,
    t3ver_tstamp int(11) unsigned DEFAULT '0' NOT NULL,
    t3ver_move_id int(11) unsigned DEFAULT '0' NOT NULL,
    t3_origuid int(11) unsigned DEFAULT '0' NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),
    KEY t3ver_oid (t3ver_oid,t3ver_wsid),
    KEY language (l10n_parent,sys_language_uid)
);

CREATE TABLE tx_touromaster_manufacturer (
     uid int(11) unsigned NOT NULL auto_increment,

     manufacturer varchar(255) DEFAULT '' NOT NULL,
     model varchar(255) DEFAULT '' NOT NULL,

     PRIMARY KEY (uid)
);

CREATE TABLE tx_touromaster_voucher (
     uid int(11) unsigned NOT NULL auto_increment,

     voucher varchar(255) DEFAULT '' NOT NULL,

     PRIMARY KEY (uid)
);