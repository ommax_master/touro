<?php

/*
 * This file is part of the package bw3rk/touro_master.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

/***************
 * TypoScript: Full Package
 * This includes the full setup including all configurations
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'touro_master',
    'Configuration/TypoScript',
    'Touro Master: Site-Package'
);