<?php

/*
 * This file is part of the package bw3rk/touro_master.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3_MODE') || die();

/***************
 * Temporary variables
 */
$extensionKey = 'touro_master';

/***************
 * Register PageTS
 */
// BackendLayouts
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $extensionKey,
    'Configuration/TsConfig/Page/Mod/WebLayout/BackendLayouts.tsconfig',
    'Touro Site-Package: Backend Layouts'
);
