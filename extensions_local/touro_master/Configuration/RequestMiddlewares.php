<?php

return [
    'frontend' => [
        'bw3rk/touro-master/test' => [
            'target' => \BW3rK\TouroMaster\Middleware\FrontendXhrRequestMiddleware::class,
            'after' => [
                'typo3/cms-frontend/content-length-headers'
            ]
        ]
    ]
];