<?php

/*
 * This file is part of the package bw3rk/touro_master.
 *
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

$EM_CONF[$_EXTKEY] = [
    'title' => 'Touro Master',
    'description' => 'Bootstrap SitePackage based on Bootstrap-Package.',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.0-10.4.99',
            'rte_ckeditor' => '9.5.0-10.4.99',
            'seo' => '9.5.0-10.4.99',
            'bootstrap_package' => '11.0.0-11.0.99',
        ],
        'conflicts' => [
            'css_styled_content' => '*',
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'BW3rK\\TouroMaster\\' => 'Classes'
        ],
    ],
    'state' => 'beta',
    'version' => '0.0.2',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Frank Klar',
    'author_email' => 'typo3@klar-it.info',
    'author_company' => 'private',
];
